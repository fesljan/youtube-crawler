#!/bin/bash

youtube-dl -g "$1" | head -n 1 | awk -F'/' '{print $3}' | xargs dig +short | tail -n1
