from selenium import webdriver
from selenium.webdriver.common.by import By
import time
import subprocess
import sys
import os
from pyvirtualdisplay import Display

def process_video(youtube_url):
    opt = webdriver.ChromeOptions()
    opt.add_argument("--disable-xss-auditor")
    opt.add_argument("--disable-web-security")
    opt.add_argument("--allow-running-insecure-content")
    opt.add_argument("--no-sandbox")
    opt.add_argument("--disable-setuid-sandbox")
    opt.add_argument("--disable-webgl")
    opt.add_argument("--disable-popup-blocking")
    opt.add_extension("./ublock.crx") # Enable ublock origin
    opt.add_extension("./YouTube-NonStop.crx")

    driver = webdriver.Chrome(options=opt)
    driver.maximize_window()
    driver.get(youtube_url)

    time.sleep(4) # Wait for page to load
    try:
        driver.find_element(by=By.XPATH,value='/html/body/ytd-app/ytd-consent-bump-v2-lightbox/tp-yt-paper-dialog/div[4]/div[2]/div[6]/div[1]/ytd-button-renderer[1]/a/tp-yt-paper-button/yt-formatted-string').click()
    finally:
        pass

    player_status = -2

    total_time=0
    while player_status != 0:
        player_status = driver.execute_script("return document.getElementById('movie_player').getPlayerState()")
        driver.save_screenshot("current.png")
        time.sleep(2)
        total_time += 2
        # Terminate watching if video is longer than 15 minutes
        if total_time >= 60*15:
            break
    driver.close()

monitor = ["ipFlowDetector", "160.217.213.146", "-f"]
#monitor = ["./monitor.bash"]
number_of_iterations_per_video = 5
file_to_parse = sys.argv[1]

print("Will parse a file {}.".format(file_to_parse))
file = open(file_to_parse)

for line in file.readlines():
    line = line.split(",")
    for iteration in range(number_of_iterations_per_video):
        videourl = "https://youtube.com/watch?v={}".format(line[0])
        playbackip = str(subprocess.run(["./youtube_playback.bash", videourl], capture_output=True).stdout.strip(), 'utf-8')
        print("Will play video (probably) from: ", playbackip)

        # Force app to use probe endpoint
        subprocess.run(["./add_to_router.bash", playbackip])

        final_file_name = "/mnt/fingerprints/" + line[0] + "." + str(iteration) + "."  + ".".join(line[1:]).replace("\n","")
        print("Plan to output: ", final_file_name)
        if os.path.exists(final_file_name) == False:
            process = subprocess.Popen(monitor + [final_file_name] + ["-o", "allowedIPs:" + playbackip])
            disp = Display(visible=False, size=(1920, 1080))
            disp.start()
            print("Processing video with id: {}, features len: {}".format(line[0], len(line)-1))
            try:
                process_video(videourl)
            finally:
                pass
            disp.stop()
            process.send_signal(2)
            process.wait()
        else:
            print("Filename: {} exists, skipping.".format(final_file_name))

